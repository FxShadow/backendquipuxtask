TRABAJO BACK-END / SEMILLERO QUIPUX

HECHO POR: SEBASTIAN VASQUEZ PARRA
T.I 1035971586

INFORMACION CARGADA A LA BASE DE DATOS

- Toda la informacion relacionada con el estudiante con id  = 6, nombre = "Sebastian Vasquez"

MODELOS CREADOS

- MATERIA
- HORARIO
- NOTA

METODOS WEBSERVICES CREADOS:

- getStudentsList
- getSubjectsList
- getSubjectsStudent
- getScheduleStudent
- getAverageGrades
- getStudentPass

DOCUMENTACION DEL CODIGO

No hay no existe :p

Rutas (PATHS) de las soluciones:

2. /servicesRest/colegio/getStudentsList
3. /servicesRest/colegio/getSubjectsStudent
4. /servicesRest/colegio/getScheduleStudent
5. /servicesRest/colegio/getAverageGrades
6. /servicesRest/colegio/getStudentPass
